<?php

Route::get('/', 'HomeController@showWelcome');
Route::get('user', 'HomeController@user');
Route::get('edit/{id}', 'HomeController@edit');

//nerd
Route::resource('nerds','NerdController');
