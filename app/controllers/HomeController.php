<?php

class HomeController extends BaseController {

	public function showWelcome()
	{
		return View::make('hello');
	}
	public function user()
	{
		$user = User::all();
    	return View::make('users',array('users'=> $user));
	}
	public function edit($id)
	{
		$user = User::find($id);
        return View::make('users',array('users' => $user));
	}

}
