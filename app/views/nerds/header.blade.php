@section('header')
<div class="container">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('nerds') }}">Home</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('nerds') }}">All Nerds</a></li>
        <li><a href="{{ URL::to('nerds/create') }}">Create a Nerd</a>
    </ul>
</div>
@stop